	//
//  NoteListTableViewController.swift
//  iPhoneSE
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 Evan le Clercq. All rights reserved.
//

import UIKit

class NoteListTableViewController: UITableViewController {
    
    let data = dbManager()
    var notes :[Note] = []
    var empID : String?
    @IBOutlet weak var addNoteButton: UIBarButtonItem!
    @IBOutlet var noteView: UITableView!
    
    override func viewDidAppear(animated: Bool) {
        self.noteView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.setDatabasePath()
        notes = data.getAllEmployeeNotes(empID!)
        if (notes.count > 0) {
            return notes.count
        } else {
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("NoteDetail", sender: tableView)
    }
    
    //TODO: Create custom cell for final version with review date and creation date of item.
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> NoteListCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("NoteCell") as! NoteListCell
        
        if (notes.count > 0) {
            let note = notes[indexPath.row]
        
            cell.noteTitle.text = note.noteTitle
            cell.noteAdded.text = note.addedDate
        } else {
            cell.noteTitle.text = "No Notes Found"
            cell.noteAdded.text = " - "
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier != "NewNoteSegue") {
            let indexPath = self.tableView.indexPathForSelectedRow!
            if (notes.count < 1) {
                print ("No Notes")
            } else {
                let noteView = segue.destinationViewController as! NoteViewController
                noteView.note = self.notes[indexPath.row]
                noteView.newNote = false
            }
        } else {
            let noteView = segue.destinationViewController as! NoteViewController
            noteView.note = Note()
            noteView.note?.empID = empID
            noteView.note?.userNote = false
            noteView.newNote = true
        }
        
    }
    
    @IBAction func addNotePressed(sender: UIBarButtonItem) {
        performSegueWithIdentifier("NewNoteSegue", sender: sender)
    }
    
}
