//
//  dbManager.swift
//  iPhoneSE
//
//  Created by Evan Le Clercq on 20/07/2016.
//  Copyright © 2016 Evan le Clercq. All rights reserved.
//

import Foundation

class dbManager {

//    var databasePath = NSString()
    var contactDB : FMDatabase?
    
    
    //DATABASE MANAGEMENT FUNCTIONS
    
    func setDatabasePath () {
        
        let path = NSBundle.mainBundle().pathForResource("EmployEaseLocal", ofType: "db")
        contactDB = FMDatabase(path: path)
        
        if (!contactDB!.open()) {
            print ("Unable to open database")
            return
        }
        
    }
    

    func openDB () {
        
        if (contactDB!.open()) {
            print ("Database opened successfully")
        } else {
            print ("Error: Unable to open database")
        }
    
    }
    
    func closeDB() {
        if (contactDB!.close()) {
            print ("Database closed successfully")
        } else {
            print ("Error: Unable to close database")
        }
    }
    
    func clearDB() {
        //TODO: remove all existing information from database
    }
    
    
    //GET ENTRIES FROM DATABASE
    
    //returns an array of all employee id numbers in the db
    func getAllEmployeeData () -> [String] {
        var empIDList = [String]()
        let sql_stmt = "SELECT id FROM employees"
       
        openDB()
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        while (results!.next()) {
            empIDList.append((results?.stringForColumn("id"))!)
        }
        
//        closeDB()
        
        return empIDList
    }
    
    //returns an array of all employee names in the db
    func getAllEmployeeNames () -> [String] {
        var empNameList = [String]()
        let sql_stmt = "SELECT first_name, last_name FROM employees"
        
        openDB()
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        while (results!.next()) {
            empNameList.append((results?.stringForColumn("first_name"))! + " " + (results?.stringForColumn("last_name"))!)
        }
        
        //        closeDB()
        
        return empNameList
    }
    
    // Returns all fields of employee where they match the given id.
    func getEmployeeFromID (id : String) -> Employee {
    
//        let sql_stmt = "SELECT * FROM employees WHERE id = '" + id + "'"
        let sql_stmt = "SELECT * FROM employees WHERE id=\(id)"
        print (sql_stmt)
        var emp = Employee()
        openDB()
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        print (results)
        
        if (results!.next()) {
        
            var linked : String = ""
            if (results?.stringForColumn("twitter") == nil || results?.stringForColumn("twitter") == "") {
                linked = "nil"
            } else {
                linked = (results?.stringForColumn("twitter"))!
            }
        
            var image : String = ""
            if (results?.stringForColumn("photo") == nil) {
                image = "nil"
            } else {
                image = (results?.stringForColumn("photo"))!
            }
        
        
            emp = Employee (
                id: (results?.stringForColumn("id"))!,
                first: (results?.stringForColumn("first_name"))!,
                last: (results?.stringForColumn("last_name"))!,
                pos: (results?.stringForColumn("position"))!,
                start: (results?.stringForColumn("start_date"))!,
                twitter: linked,
                photo: image
            )
        }
        
        return emp
        
//        closeDB()

    }
    
    // Returns array of all skills for the given employee id
    func getEmployeeSkills (id : String) -> [Skill] {
        
        var empSkills = [Skill]()
        let sql_stmt = "Select * from employee_skills WHERE emp_id=\(id)"
        
//        openDB()
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        while (results?.next() == true) {
            empSkills.append(Skill (
                skill: (results?.stringForColumn("skill"))!,
                emp:(results?.stringForColumn("emp_id"))!,
                id: (results?.stringForColumn("id"))!
                ))
        }
        
//        closeDB()
        
        return empSkills
        
    }
    
    // Returns array of all notes for given employee id
    
    func getEmployeeNotes (id : String) -> [Note] {
        
        var empNotes = [Note]()
        let sql_stmt = "SELECT * from notes WHERE emp_id=\(id)"
        
        openDB()
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        while (results?.next() == true) {

            empNotes.append(Note (
                emp: (results?.stringForColumn("emp_id"))!,
                note: (results?.stringForColumn("id"))!,
                added: (results?.stringForColumn("added_date"))!,
                review: (results?.stringForColumn("review_date"))!,
                text: (results?.stringForColumn("note_body"))!,
                title: (results?.stringForColumn("note_title"))!,
                user: false
                ))
        }
        
        closeDB()
        
        return empNotes
        
    }
    
    //USER RELATED FUNCTIONS
    func updateUserInformation () {
        //TODO: Edit the user specific information
    }
    
    
    //EMPLOYEE RELATED FUNCTIONS
    func addEmployee (emp : Employee) {
        let sql_stmt = "INSERT INTO employees (first_name, last_name, position, start_date, twitter, photo) VALUES ('\(emp.firstname!)', '\(emp.lastname!)', '\(emp.position!)', '\(emp.start_date!)', '\(emp.twitter!)', '\(emp.photo!)')"
        print (sql_stmt)
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Employee added successfully")
        } else {
            print ("Employee unable to be added")
        }
        
        closeDB()
        
    }
    
    func removeEmployee(emp: Employee) {
        
        let sql_stmt = "DELETE FROM emplyees WHERE id=\(emp.id)"
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Entry successfully deleted")
        } else {
            print ("Entry Unable to be deleted")
        }
        
        closeDB()
        
    }
    
    func editEMployee(emp : Employee) {

        let sql_stmt = "UPDATE employees SET first_name='\(emp.firstname)', last_name='\(emp.lastname), position='\(emp.position)', twitter='\(emp.twitter)' WHERE id=\(emp.id)"
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Employee Sucessfully Updated")
        } else {
            print ("Employee Unable to be Updated")
        }
        
        closeDB()
    
    }
    
    
    //NOTE RELATED FUNCTIONS
    func addNote (note : Note) {
        
        var user:String
        if (note.userNote! == true) {
            user = "true"
        } else {
            user = "false"
        }
        
        let sql_stmt = "INSERT INTO notes (emp_id, added_date, note_body, user_note) VALUES ('\(note.empID!)', '\(note.addedDate!)', '\(note.text!)', '\(user)')"
        print (sql_stmt)
        
        openDB()
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Note Successfully Added")
        } else {
            print ("Note Unable to be added")
        }
        closeDB()
        
    }
    
    func removeNote (note : Note) {
        
        let sql_stmt = "DELETE FROM notes WHERE id=\(note.noteID)"
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Note Successfully Deleted")
        } else {
            print ("Note Unable to be deleted")
        }
        
        closeDB()
        
    }
    
    func editNote (note : Note) {

        let id =  Int(note.noteID!)
        let sql_stmt = "UPDATE notes SET review_date='\(note.reviewDate!)', note_body='\(note.text!)', note_title='\(note.noteTitle!)' WHERE id=\(id!)"
        print (sql_stmt)
        print (note.noteTitle!)
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Note Updated Successfully")
        } else {
            print ("Note Unable to be Updated")
        }
        
        closeDB()
        
    }
    
    func getAllNotes (user : Bool) -> [Note] {
        
        openDB()
        
        var notes = [Note]()
        var userStr : String = "false"
        
        if (user) {
            userStr = "true"
        }
        
        let sql_stmt = "SELECT * FROM notes WHERE user_note = '\(userStr)'"
        print (sql_stmt)
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        if (results!.next()) {
            
            var empID :String
            if (results?.stringForColumn("emp_id") == nil) {
                empID = ""
            } else {
                empID = (results?.stringForColumn("emp_id"))!
            }
            
            var reviewDate :String
            if (results?.stringForColumn("review_date") == nil) {
                reviewDate = ""
            } else {
                reviewDate = (results?.stringForColumn("review_date"))!
            }
            
            var userNote :Bool
            if (results?.stringForColumn("user_note") == "false") {
                userNote = false
            } else {
                userNote = true
            }
            
            notes.append ( Note (
                emp: empID,
                note: (results?.stringForColumn("id"))!,
                added: (results?.stringForColumn("added_date"))!,
                review: reviewDate,
                text: (results?.stringForColumn("note_body"))!,
                title: (results?.stringForColumn("note_title"))!,
                user: userNote
            ))
        }
        
        return notes
        
    }
    
    
    func getAllEmployeeNotes (id : String) -> [Note] {
        
        openDB()
        
        var notes = [Note]()
        
        let sql_stmt = "SELECT * FROM notes WHERE emp_id = '\(id)'"
        print (sql_stmt)
        
        let results:FMResultSet? = contactDB!.executeQuery(sql_stmt, withArgumentsInArray: nil)
        
        if (results!.next()) {
            
            var empID :String
            if (results?.stringForColumn("emp_id") == nil) {
                empID = ""
            } else {
                empID = (results?.stringForColumn("emp_id"))!
            }
            
            var reviewDate :String
            if (results?.stringForColumn("review_date") == nil) {
                reviewDate = ""
            } else {
                reviewDate = (results?.stringForColumn("review_date"))!
            }
            
            var userNote :Bool
            if (results?.stringForColumn("user_note") == "false") {
                userNote = false
            } else {
                userNote = true
            }
            
            notes.append ( Note (
                emp: empID,
                note: (results?.stringForColumn("id"))!,
                added: (results?.stringForColumn("added_date"))!,
                review: reviewDate,
                text: (results?.stringForColumn("note_body"))!,
                title: (results?.stringForColumn("note_title"))!,
                user: userNote
                ))
        }
        
        return notes
        
    }
    
    
    //SKILL RELATED FUNCTIONS
    func addSkill (skill : Skill) {
        
        let sql_stmt = "INSERT INTO employee_skills (id, emp_id, skill) VALUES ('\(skill.skillID)', '\(skill.empID)', '\(skill.skillDesc)'"
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Skill Successfully Added")
        } else {
            print ("Skill Unable to be Added")
        }
        
        closeDB()
        
    }
    
    func removeSkill (skill : Skill) {
        let sql_stmt = "DELETE FROM employee_skills WHERE id = '\(skill.skillID)'"
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Skill Successfully Deleted")
        } else {
            print ("Skill Unable to be deleted")
        }
        
        closeDB()
        
    }
    
    func editSkill (skill : Skill) {
        
        let sql_stmt = "UPDATE employee_skills SET skill='\(skill.skillDesc)' WHERE id = '\(skill.skillID)'"
        
        openDB()
        
        if (contactDB!.executeStatements(sql_stmt)) {
            print ("Skill Updated Successfully")
        } else {
            print ("Skill Unable to be Updated")
        }
        
        closeDB()
        
    }
    
}


