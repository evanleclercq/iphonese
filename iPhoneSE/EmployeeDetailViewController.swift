//
//  EmployeeDetailViewController.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//


//TODO: incorporate the NoteListView into the EmployeeDetailView so under the scroll of the employee details
//      a ist of all of the notes relevant to that employee are also there.
import Foundation
import UIKit

class EmployeeDetailViewController: UIViewController {
    
    var data = dbManager()
    
    var emp : Employee?

    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var empImage: UIImageView!
    @IBOutlet weak var lblSkill: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var noteButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data.setDatabasePath()
        let skills = data.getEmployeeSkills((emp?.id)!)
        
        var skillsOutput: String? = ""
        
        for i in 0 ..< skills.count {
            skillsOutput = skillsOutput! + skills[i].skillDesc! + "\n"
        }
        
        lblFirstName.text = emp?.firstname
        lblLastName.text = emp?.lastname
        empImage.image = UIImage (named: (emp?.photo)!)
        lblSkill.text = skillsOutput
        lblPosition.text = emp?.position
        lblStartDate.text = emp?.start_date
        
        
        //set look for note button
        noteButton.layer.borderWidth = 1.0
        noteButton.layer.cornerRadius = 8
        noteButton.layer.backgroundColor = UIColor.blueColor().CGColor
        noteButton.layer.borderColor = UIColor.blackColor().CGColor
        noteButton.layer.shadowColor = UIColor.lightGrayColor().CGColor
        

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier != "twitterSegue"){
        let noteList = segue.destinationViewController as! NoteListTableViewController
        noteList.empID = self.emp?.id
        } else {
        let twitterFeed = segue.destinationViewController as! TwitterViewController
        twitterFeed.userHandle = self.emp!.twitter
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}			