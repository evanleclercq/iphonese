//
//  CalendarEvents.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//

// File will get calendar events from Calendar using Event Kit

import Foundation

enum CalendarEvents: Int {
    case date1 = 1
    case date2
    case date3
    case date4
    case date5
    
    init?(number: Int)
    {
        switch number
        {
        case 1: self = .date1
        case 2: self = .date2
        case 3: self = .date3
        case 4: self = .date4
        case 5: self = .date5
        default:
            return nil
        }
    }

    
    var id: Int {
        get {
            return self.rawValue
        }
    }
    
    //would be NSDate in final implementation
    /*var date: String {
        get {
            switch self {
            case .date1: return "01/07/2016"
            case .date2: return "02/07/2016"
            case .date3: return "03/07/2016"
            case .date4: return "05/07/2016"
            case .date5: return "16/07/2016"
            }
        }
    }*/
    
    var date: NSDate {
        get {
            switch self {
            case .date1: return NSDate(timeIntervalSinceReferenceDate: 489067200.0)
            case .date2: return NSDate(timeIntervalSinceReferenceDate: 489412800.0)
            case .date3: return NSDate(timeIntervalSinceReferenceDate: 489884800.0)
            case .date4: return NSDate(timeIntervalSinceReferenceDate: 493646400.0)
            case .date5: return NSDate(timeIntervalSinceReferenceDate: 493819200.0)
            }
        }
    }
    
    var employee: String {
        get {
            switch self {
            case .date1: return "Bill Jones"
            case .date2: return "James Getty"
            case .date3: return "Melissa Black"
            case .date4: return "Julia Smith"
            case .date5: return "Julia Smith"
            }
        }
    }
    
    var note: String {
        get {
            switch self {
            case .date1: return "Performance Review"
            case .date2: return "Pay Discussion"
            case .date3: return "Project Discussion"
            case .date4: return "Performance Review"
            case .date5: return "Dismissal"
            }
        }
    }
    
    static func getEvents() -> [CalendarEvents] {
    return [date1,
            date2,
            date3,
            date4,
            date5]
        }
    static func getPastEvents () ->[CalendarEvents]{
        var events = [date1, date2, date3, date4, date5]
        
        events = events.filter{events in events.date.timeIntervalSinceReferenceDate < NSDate().timeIntervalSinceReferenceDate}
        return events
    }
    
    static func getFutureEvents () ->[CalendarEvents]{
        var events = [date1, date2, date3, date4, date5]
        
        events = events.filter{events in events.date.timeIntervalSinceReferenceDate >= NSDate().timeIntervalSinceReferenceDate}
        return events
    }
}