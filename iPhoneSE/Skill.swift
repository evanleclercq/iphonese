//
//  Skill.swift
//  iPhoneSE
//
//  Created by Evan Le Clercq on 10/08/2016.
//  Copyright © 2016 Evan le Clercq. All rights reserved.
//

import Foundation

class Skill {
    
    var skillDesc : String?
    var empID : String?
    var skillID : String?
    
    init (skill:String, emp:String, id:String) {
        self.skillDesc = skill
        self.empID = emp
        self.skillID = id
    }
    
}
