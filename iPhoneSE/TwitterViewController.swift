//
//  TwitterViewController.swift
//  iPhoneSE
//
//  Created by Peter Tomlinson on 20/08/2016.
//  Copyright © 2016 Evan le Clercq. All rights reserved.
//
// Adapted from http://www.techotopia.com/index.php/An_Swift_iOS_8_Twitter_Integration_Tutorial_using_SLRequest
// Must set twitter account on device.  Login: employeaseapp   PW: iphonese

import UIKit
import Social
import Accounts

class TwitterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var userHandle:String!
    @IBOutlet weak var twitterTable: UITableView!
    var dataSource = [AnyObject]()
    
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.twitterTable.registerClass(UITableViewCell.self,forCellReuseIdentifier: "Cell")
        self.getTimeLine()
        self.twitterTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTimeLine() {
        
        let account = ACAccountStore()
        let accountType = account.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
        
        account.requestAccessToAccountsWithType(accountType, options: nil,completion: {(success: Bool, error: NSError!) -> Void in
                                                    
        if success {
            let arrayOfAccounts = account.accountsWithAccountType(accountType)
            if arrayOfAccounts.count > 0 {
                let twitterAccount = arrayOfAccounts.last as! ACAccount
                let requestURL = NSURL(string:"https://api.twitter.com/1.1/statuses/user_timeline.json")
                let parameters = ["screen_name":self.userHandle,"include_rts":"0","trim_user": "1","count":"20"]
                let postRequest = SLRequest(forServiceType:SLServiceTypeTwitter,requestMethod: SLRequestMethod.GET,URL: requestURL,parameters: parameters)
                postRequest.account = twitterAccount
                postRequest.performRequestWithHandler(
                    {(responseData: NSData!,urlResponse: NSHTTPURLResponse!,error: NSError!) -> Void in
                
                self.dataSource = try! NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableLeaves) as! [AnyObject]
                if self.dataSource.count != 0 {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.twitterTable.reloadData()
                    }
                }
            })
          }
        }   else {
                self.errorLabel.hidden = false
            }
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.twitterTable.dequeueReusableCellWithIdentifier("Cell")!as UITableViewCell
        let row = indexPath.row
        let tweet = self.dataSource[row] as! NSDictionary
        cell.textLabel!.text = tweet.objectForKey("text") as? String
        cell.textLabel!.numberOfLines = 0
        return cell
    }
    
}