//
//  NewEmployeeViewController.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//


import UIKit
import EventKit

class NewEmployeeViewController : UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var emp: Employee?
    let data = dbManager()
    var savePath:String = ""
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var positionField: UITextField!
    @IBOutlet weak var skillField: UITextField!
    
    @IBOutlet weak var imagePicked: UIImageView!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    
    override func viewDidLoad() {
        
        //Set Button Styles
        saveButton.layer.borderWidth = 1.0
        saveButton.layer.cornerRadius = 8
        saveButton.layer.backgroundColor = UIColor.blueColor().CGColor
        saveButton.layer.borderColor = UIColor.blackColor().CGColor
        saveButton.layer.shadowColor = UIColor.lightGrayColor().CGColor
        
        cancelButton.layer.borderWidth = 1.0
        cancelButton.layer.cornerRadius = 8
        cancelButton.layer.backgroundColor = UIColor.redColor().CGColor
        cancelButton.layer.borderColor = UIColor.blackColor().CGColor
        cancelButton.layer.shadowColor = UIColor.lightGrayColor().CGColor
        
        imagePicked.image = UIImage (named: "employees")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewEmployeeViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }
    //camera function
    @IBAction func photoButtonPressed(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    //image picker function
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [NSObject : AnyObject]!) {
        imagePicked.image = image
        
        //save image
        let imageData = UIImagePNGRepresentation(image)
        savePath = getDocumentsDirectory().stringByAppendingPathComponent(firstNameField.text! + lastNameField.text! + ".png")
        let result = imageData!.writeToFile(savePath, atomically: true)
        
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func CancelButtonPressed(sender: AnyObject) {
            self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func SaveButtonPressed(sender: AnyObject) {
        
        var photo = "employees"
        if(savePath != ""){
            photo = savePath
        }
        //save entered fields
        emp?.firstname = firstNameField.text
        emp?.lastname = lastNameField.text
        emp?.position = positionField.text
        emp?.photo = photo
        emp?.twitter = ""
        emp?.start_date = ""
        
        data.setDatabasePath()
        data.addEmployee(emp!)
    
        self.navigationController?.popToRootViewControllerAnimated(true)
        
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
}