    //
    //  PersonalNoteListTableViewController.swift
    //  iPhoneSE
    //
    //  Created by Evan Le Clercq - S3516635
    //             Brent Varischetti - S3387956
    //             Peter Tomlinson - S3384711
    //  Copyright © 2016 Evan le Clercq. All rights reserved.
    //
    
    import UIKit
    
    class PersonalNoteListTableViewController: UITableViewController {
        
        let	 data = dbManager()
        var notes : [Note] = []
        @IBOutlet weak var addNoteButton: UIBarButtonItem!
        @IBOutlet var noteView: UITableView!
        
        override func viewDidAppear(animated: Bool) {
            self.noteView.reloadData()
        }
        
        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if (notes.count > 0) {
                return notes.count
            } else {
                return 1
            }
        }
        
        //TODO: Create custom cell for final version with review date and creation date of item.
        override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> PersonalNoteListCell {
                        
            let cell = self.tableView.dequeueReusableCellWithIdentifier("PersonalNoteListCell") as! PersonalNoteListCell
            
            if (notes.count > 0) {
                let note = notes[indexPath.row]
            
                cell.noteTitle.text = note.noteTitle
                cell.noteAdded.text = note.addedDate
            } else {
                cell.noteTitle.text = "No Notes Found"
                cell.noteAdded.text = " - "
            }
            
            return cell
        }
        
        override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            performSegueWithIdentifier("NoteDetailSegue", sender: indexPath)
        }
        
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
            if (segue.identifier == "NoteDetailSegue") {
                let indexPath = self.tableView.indexPathForSelectedRow!
                if (notes.count < 1) {
                    print ("No Notes")
                } else {
                    let personalNV = segue.destinationViewController as! PersonalNoteViewController
                    personalNV.note = self.notes[indexPath.row]
                    personalNV.newNote = false
                }
            } else {
                let personalNV = segue.destinationViewController as! PersonalNoteViewController
                personalNV.note = Note()
                personalNV.note!.empID = ""
                personalNV.note!.userNote = true
                personalNV.newNote = true
            }
        }
        
        
        @IBAction func addButtonPressed(sender: AnyObject) {
            performSegueWithIdentifier("NewNoteSegue", sender: sender)
        }
        
    }
