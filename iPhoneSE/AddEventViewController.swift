//
//  AddEventViewController.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//


import UIKit
import EventKit

class AddEventViewController: UIViewController{
    
    @IBOutlet weak var employeePicker: UIPickerView!
    @IBOutlet weak var eventTypePicker: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var employeeLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    var addEventEmployee = ""
    var addEventType = ""
    var calendars: [EKCalendar]?
    var calendar: EKCalendar!
    let data = dbManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddEventViewController.dismissPickers))
        view.addGestureRecognizer(tap)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
            return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        data.setDatabasePath()
        let eventEmployees = data.getAllEmployeeNames()
        
        if pickerView == employeePicker {
            return eventEmployees.count
        } else if pickerView == eventTypePicker{
            return eventTypes.count
        } else{
            return 1
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        data.setDatabasePath()
        let eventEmployees = data.getAllEmployeeNames()
        
    if pickerView == employeePicker {
            addEventEmployee = eventEmployees[row]
            employeeLabel.text = eventEmployees[row]
            return eventEmployees[row]
        } else if pickerView == eventTypePicker{
            addEventType = eventTypes[row]
            typeLabel.text = eventTypes[row]
            return eventTypes[row]
        } else {
            return eventTypes[row]
        }
    }
    @IBAction func datePickerChanged(sender: AnyObject) {
        dateLabel.text = dateToString(datePicker.date)
    }
    
    @IBAction func setDate(sender: AnyObject) {
        datePicker.hidden = false
        employeePicker.hidden = true
        eventTypePicker.hidden = true
        dateLabel.textColor = UIColor.blackColor()
    }
    @IBAction func setEmployee(sender: AnyObject) {
        employeePicker.hidden = false
        datePicker.hidden = true
        eventTypePicker.hidden = true
        employeeLabel.textColor = UIColor.blackColor()
        
    }
    @IBAction func setType(sender: AnyObject) {
        eventTypePicker.hidden = false
        datePicker.hidden = true
        employeePicker.hidden = true
        typeLabel.textColor = UIColor.blackColor()
    }
    
    
    @IBAction func addEventButton(sender: UIButton) {
        let eventStore = EKEventStore();
        // Use Event Store to create a new calendar instances
         if let calendarForEvent = eventStore.calendarWithIdentifier(self.calendar.calendarIdentifier)
        {
            let newEvent = EKEvent(eventStore: eventStore)
            
            newEvent.calendar = calendarForEvent
            newEvent.title = addEventEmployee
            newEvent.notes = addEventType
            newEvent.startDate = self.datePicker.date
            newEvent.endDate = self.datePicker.date.dateByAddingTimeInterval(60.0*60.0)
            do {
                try eventStore.saveEvent(newEvent, span: .ThisEvent, commit: true)
                performSegueWithIdentifier("SaveEventSegue", sender: self)
            } catch {
                let alert = UIAlertController(title: "Event could not save", message: (error as NSError).localizedDescription, preferredStyle: .Alert)
                let OKAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(OKAction)
                
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    func dateToString(date: NSDate) ->String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        return dateFormatter.stringFromDate(date)
    }
    
    func dismissPickers() {
        datePicker.hidden = true
        employeePicker.hidden = true
        eventTypePicker.hidden = true
    }
        
    
}

