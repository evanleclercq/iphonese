//
//  Employee.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//

import Foundation

class Employee {
 
    var id : String?
    var firstname : String?
    var lastname : String?
    var position : String?
    var start_date : String?
    var twitter : String?
    var photo : String?
    
    init (){}
    
    init (id:String, first:String, last:String, pos:String, start:String, twitter:String, photo:String) {
        self.id = id
        self.firstname = first
        self.lastname = last
        self.position = pos
        self.start_date = start
        self.twitter = twitter
        self.photo = photo
    }
    
    func getFullName () -> String {
        return self.firstname! + " " + self.lastname!
    }
    
}

    