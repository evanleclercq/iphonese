//
//  CalendarViewController.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//

import UIKit
import EventKit


class CalendarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let eventStore = EKEventStore()
    var calendars: [EKCalendar]?
    var eeCal: EKCalendar?
    var events: [EKEvent] = []
    
    @IBOutlet weak var EventDetail: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var eventsTable: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accessCalendars()
        
        //create calendar if it hasn't already been created
        var eeCalExists = false
        for calendar in calendars!{
            if calendar.title == "EmployEase"{
              eeCalExists = true
                eeCal = calendar
            }
        }
        if eeCalExists == false{
        createCalendar()
        }
        events = getFutureEventsNew()
        eventsTable.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(tableView: UITableView,
                   cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("BasicCell")
        var event : EKEvent
        event = events[indexPath.row]
        if (cell == nil) {
            cell = UITableViewCell(
                style: UITableViewCellStyle.Subtitle,
                reuseIdentifier: "BasicCell")
        }
        
        cell?.textLabel?.text = dateToString(event.startDate)
        cell?.detailTextLabel?.text = event.title
        
        return cell!
    }
    
    func tableView(tableView: UITableView,
                   didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var event : EKEvent
        event = events[indexPath.row]
        EventDetail.text = event.notes
    }
    
    @IBAction func indexChanged(sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            events = getEventsNew()
        case 1:
            events = getPastEventsNew()
        case 2:
            events = getFutureEventsNew()
        default:
            events = getFutureEventsNew()
        }
        eventsTable.reloadData()
        
    }
    
    func dateToString(date: NSDate) ->String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.stringFromDate(date)
    }
    
    
    func accessCalendars() {
        let status = EKEventStore.authorizationStatusForEntityType(EKEntityType.Event)
        switch (status) {
        case EKAuthorizationStatus.NotDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.Authorized:
            // Things are in line with being able to show the calendars in the table view
            loadCalendars()
            //refreshTableView()
        case EKAuthorizationStatus.Restricted, EKAuthorizationStatus.Denied:
            // We need to help them give us permission
            let alertController = UIAlertController(title: "Error", message:
                "Please allow calendar access in settings", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    
    func requestAccessToCalendar() {
        eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
            (accessGranted: Bool, error: NSError?) in
            
            if accessGranted == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadCalendars()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    
                })
            }
        })
    }
    func loadCalendars() {
        self.calendars = eventStore.calendarsForEntityType(EKEntityType.Event)
    }
    
    func createCalendar() {
        
        accessCalendars()
        let eeCal = EKCalendar(forEntityType: .Event, eventStore: eventStore)
        
        eeCal.title = "EmployEase"
        
        let sourcesInEventStore = eventStore.sources
        
        //check if being run on simulator or device
        if Platform.isSimulator {
            eeCal.source = sourcesInEventStore.filter{
                (source: EKSource) -> Bool in
                source.sourceType.rawValue == EKSourceType.Local.rawValue
                }.first!
        } else {
            eeCal.source = sourcesInEventStore.filter{
                (source: EKSource) -> Bool in
                source.sourceType.rawValue == EKSourceType.MobileMe.rawValue
                }.first!
        }
        do {
            try eventStore.saveCalendar(eeCal, commit: true)
            NSUserDefaults.standardUserDefaults().setObject(eeCal.calendarIdentifier, forKey: "EventTrackerPrimaryCalendar")
        } catch {
            let alert = UIAlertController(title: "Calendar could not save", message: (error as NSError).localizedDescription, preferredStyle: .Alert)
            let OKAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alert.addAction(OKAction)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func getEventsNew() -> [EKEvent]{
        let eventStore = EKEventStore()
        let start = NSDate(timeIntervalSinceNow: -366*24*3600)
        let end = NSDate(timeIntervalSinceNow: +366*24*3600)

        let predicate = eventStore.predicateForEventsWithStartDate(start, endDate: end, calendars: [eeCal!])
        
        let events = eventStore.eventsMatchingPredicate(predicate)
        return events
    }
    
    func getFutureEventsNew() -> [EKEvent]{
        let eventStore = EKEventStore()
        let start = NSDate()
        let end = NSDate(timeIntervalSinceNow: +366*24*3600)
        
        let predicate = eventStore.predicateForEventsWithStartDate(start, endDate: end, calendars: [eeCal!])
        
        let events = eventStore.eventsMatchingPredicate(predicate)
        return events
    }
    
    func getPastEventsNew() -> [EKEvent]{
        let eventStore = EKEventStore()
        let start = NSDate(timeIntervalSinceNow: -366*24*3600)
        let end = NSDate()
        
        let predicate = eventStore.predicateForEventsWithStartDate(start, endDate: end, calendars: [eeCal!])
        
        let events = eventStore.eventsMatchingPredicate(predicate)
        return events
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let eventController = segue.destinationViewController as! AddEventViewController
        eventController.calendar = eeCal
    }
    
    //struct to check for platform to avoid crashes
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
                isSim = true
            #endif
            return isSim
        }()
    }

    
}

