//
//  Note.swift
//  iPhoneSE
//
//  Created by Evan Le Clercq on 10/08/2016.
//  Copyright © 2016 Evan le Clercq. All rights reserved.
//

import Foundation

class Note {
    
    var empID : String?
    var noteID : String?
    var addedDate : String?
    var reviewDate : String?
    var text : String?
    var noteTitle : String?
    var userNote : Bool?
    
    init(){}
    
    init (emp:String, note:String, added:String, review:String, text:String, title:String, user:Bool) {
        self.empID = emp
        self.noteID = note
        self.addedDate = added
        self.reviewDate = review
        self.text = text
        self.noteTitle = title
        self.userNote = user
    }
    
}
