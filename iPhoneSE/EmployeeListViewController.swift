//
//  EmployeeListViewController.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//

// Icon sourced from Icons 8: https://icons8.com


import UIKit

class EmployeeListViewController: UITableViewController {
    
    let data = dbManager()
    var tableList : [String] = []
    @IBOutlet weak var newEmployeeButton: UIBarButtonItem!
    @IBOutlet var employeeList: UITableView!
    
    
    override func viewDidAppear(animated: Bool) {
        self.employeeList.reloadData()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.setDatabasePath()
        return data.getAllEmployeeData().count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        tableList = data.getAllEmployeeData()
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell!
        
        let emp = data.getEmployeeFromID(tableList[indexPath.row])
        
        cell.textLabel!.text = emp.getFullName()
        
        let image = UIImage(named: emp.photo!)
        cell.imageView?.image = image
        
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("employeeDetail", sender: tableView)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "employeeDetail"){
            let employeeDetailViewController = segue.destinationViewController as UIViewController
            let indexPath = self.tableView.indexPathForSelectedRow!
            let selectedEmp = data.getEmployeeFromID(self.tableList[indexPath.row])
            let destinationTitle = selectedEmp.getFullName()
        
            employeeDetailViewController.title = destinationTitle
            let detailsVC = segue.destinationViewController as! EmployeeDetailViewController
            detailsVC.emp = selectedEmp
        } else if (segue.identifier == "NewEmployeeSegue"){
            let newEmployeeView = segue.destinationViewController as! NewEmployeeViewController
            newEmployeeView.title = "New Employee"
            newEmployeeView.emp = Employee()
        }
        
    }
    
    @IBAction func newEmployeePressed(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("NewEmployeeSegue", sender: self)
    }
    
}



