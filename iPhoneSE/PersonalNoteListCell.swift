//
//  NoteListCell.swift
//  iPhoneSE
//
//  Created by Phoenix on 7/07/2016.
//  Copyright © 2016 Evan le Clercq. All rights reserved.
//

import UIKit

class PersonalNoteListCell : UITableViewCell {
    
    @IBOutlet weak var noteTitle : UILabel!
    @IBOutlet weak var noteAdded : UILabel!
    
}
