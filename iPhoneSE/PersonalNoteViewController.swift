//
//  NoteViewController.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//

import UIKit

class PersonalNoteViewController: UIViewController {
    
    var data = dbManager()
    var note : Note?
    var newNote = true
    var emp : Employee?
    
    @IBOutlet weak var noteTitleField: UITextField!
    @IBOutlet weak var noteBodyField: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        
        if (newNote == true) {
            print ("Adding Note")
            note?.addedDate = "CURRENT_DATE"
            note?.noteTitle = noteTitleField.text
            note?.text = noteBodyField.text
            note?.userNote = true
            
            data.setDatabasePath()
            data.addNote((note)!)
            
        } else {
            note?.noteTitle = noteTitleField.text
            note?.text = noteBodyField.text
            note?.userNote = true
        
            data.setDatabasePath()
            data.editNote((note)!)
        }
            self.navigationController?.popToRootViewControllerAnimated(true)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (emp != nil) {newNote = false}
        
        //set look for the textview
        noteBodyField.layer.borderWidth = 1.0
        noteBodyField.layer.cornerRadius = 8
        noteBodyField.layer.borderColor = UIColor.grayColor().CGColor
        
        //set look for save button
        saveButton.layer.borderWidth = 1.0
        saveButton.layer.cornerRadius = 8
        saveButton.layer.backgroundColor = UIColor.blueColor().CGColor
        saveButton.layer.borderColor = UIColor.blackColor().CGColor
        saveButton.layer.shadowColor = UIColor.lightGrayColor().CGColor
        
        cancelButton.layer.borderColor = UIColor.blackColor().CGColor
        cancelButton.layer.borderWidth = 1.0
        cancelButton.layer.cornerRadius = 8
        cancelButton.layer.backgroundColor = UIColor.redColor().CGColor
        cancelButton.layer.shadowColor = UIColor.lightGrayColor().CGColor
        
        
        if (note != nil) {
            self.title = "Edit Note"
            self.noteTitleField.text = note!.noteTitle
            self.noteBodyField.text = note!.text
        } else {
            self.title = "New Note"
            self.noteBodyField.text = ""
            note = Note()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

