//
//  iPhoneSEUITests.swift
//  iPhoneSE
//  Group 19
//
//  Created by Evan Le Clercq - S3516635
//             Brent Varischetti - S3387956
//             Peter Tomlinson - S3384711
//  Copyright © 2016 iPhone Software Engineering - Group 19. All rights reserved.
//

import XCTest

class iPhoneSEUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    // TEST 1: test the tab bar controller is working and no errors in initial views
    func testCanNavigateTabs() {
        let app = XCUIApplication()
        app.tabBars.buttons["Calendar"].tap()
        app.tabBars.buttons["Personal Notes"].tap()
        app.tabBars.buttons["Employees"].tap()
    }
    
    // TEST 2: test that an event is added and shows in table
    func testCanAddEvent() {
        let app = XCUIApplication()
        app.tabBars.buttons["Calendar"].tap()
        app.segmentedControls.buttons["All"].tap()
        let tablesQuery = app.tables
        let eventCount1 = Int(tablesQuery.cells.count)
        app.navigationBars.buttons["Add"].tap()
        app.buttons["Date"].tap()
        app.buttons["Employee"].tap()
        app.buttons["Type"].tap()
        app.buttons["Add"].tap()
        app.segmentedControls.buttons["All"].tap()
        let eventCount2 = Int(tablesQuery.cells.count)
        XCTAssertNotEqual(eventCount1, eventCount2)
    }
    
    // TEST 3: test that twitter feed has loaded
    func testTwitterFeed() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.cells.elementBoundByIndex(0).tap()
        app.buttons["Twitter"].tap()
        let twitterTable = app.tables
        XCTAssertTrue(twitterTable.cells.elementBoundByIndex(0).exists)
        
    }
    
    // TEST 4: test that an employee can be added
    func testAddEmployee() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let employeeCount1 = Int(tablesQuery.cells.count)
        app.navigationBars.buttons["Add"].tap()
        app.buttons["Save"].tap()
        let employeeCount2 = Int(tablesQuery.cells.count)
        XCTAssertNotEqual(employeeCount1, employeeCount2)
    }
    

}
